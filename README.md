# rapidpods-team-maa-project-infoextractqa

## Add your files

- [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/sai.j.mihir/rapidpods-team-maa-project-infoextractqa.git
git branch -M main
git push -uf origin main
```
***

## PwC Information Extraction System (P.I.E)


## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.


## Visuals


## Installation


## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.


## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.


## Authors and acknowledgment
### Team Members:
* [Abhinav Prabhakar] (abhinav.prabhakar@pwc.com)
* [Sai Mihir J] (sai.j.mihir@pwc.com) 
* [Varun Bukkala] (bukkala.varun@pwc.com)
* [Laxman Tirumalasetty] (laxman.tirumalasetty@pwc.com) 

## License
MIT License
Copyright (c) 2023 Sai Mihir (IN)

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
